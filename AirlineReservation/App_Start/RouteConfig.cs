﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AirlineReservation
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{flyingFrom}/{flyingTo}/{flyingDate}/{flyingClass}/{flyingCost}/{flyingDateReturn}/{flightCode}",
                defaults: new { controller = "Airline", action = "Index", flyingFrom = UrlParameter.Optional, flyingTo = UrlParameter.Optional, flyingDate = UrlParameter.Optional, flyingClass = UrlParameter.Optional, flyingCost = UrlParameter.Optional, flyingDateReturn = UrlParameter.Optional, flightCode = UrlParameter.Optional }
            );
        }
    }
}
