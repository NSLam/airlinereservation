﻿using System.Web;
using System.Web.Optimization;

namespace AirlineReservation
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js",
                        "~/Scripts/menu_jquery.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css",
                      "~/Template/css/flexslider.css",
                      "~/Template/css/style.css"));
            bundles.Add(new Bundle("~/bundles/angular").Include(
                      "~/Scripts/angular.js",
                      "~/Scripts/moment.js"));

            //bundles.Add(new ScriptBundle("~/bundles/angular").Include(
            //          "~/Scripts/angular.js",
            //          "~/Scripts/moment.js"));

            bundles.Add(new Bundle("~/bundles/js").Include(
               "~/Scripts/AirlineReservation/app.js",
               "~/Scripts/AirlineReservation/Service/httpMethodsService.js",
               "~/Scripts/AirlineReservation/Controller/layout.controller.js"));

            //bundles.Add(new ScriptBundle("~/bundles/js").Include(
            //          "~/Scripts/AirlineReservation/app.js",
            //          "~/Scripts/AirlineReservation/Service/httpMethodsService.js"));
        }
    }
}
