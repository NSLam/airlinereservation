namespace AirlineReservation.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Booking",
                c => new
                    {
                        BookingCode = c.String(nullable: false, maxLength: 128),
                        TimeReservation = c.DateTime(nullable: false),
                        TotalAmount = c.Decimal(precision: 18, scale: 2),
                        Status = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.BookingCode);
            
            CreateTable(
                "dbo.Flight",
                c => new
                    {
                        FlightCode = c.String(nullable: false, maxLength: 128),
                        From = c.String(nullable: false, maxLength: 128),
                        To = c.String(nullable: false, maxLength: 128),
                        Date = c.DateTime(nullable: false),
                        Class = c.String(nullable: false, maxLength: 128),
                        Cost = c.String(nullable: false, maxLength: 128),
                        Time = c.DateTime(nullable: false),
                        NumberOfSeats = c.Int(nullable: false),
                        Price = c.Decimal(precision: 18, scale: 2),
                    })
                .PrimaryKey(t => new { t.FlightCode, t.From, t.To, t.Date, t.Class, t.Cost });
            
            CreateTable(
                "dbo.FlightDetails",
                c => new
                    {
                        FlightCode = c.String(nullable: false, maxLength: 128),
                        BookingCode = c.String(nullable: false, maxLength: 128),
                        Date = c.DateTime(nullable: false),
                        Class = c.String(),
                        Cost = c.String(),
                    })
                .PrimaryKey(t => new { t.FlightCode, t.BookingCode, t.Date });
            
            CreateTable(
                "dbo.Passenger",
                c => new
                    {
                        BookingCode = c.String(nullable: false, maxLength: 128),
                        Nickname = c.String(),
                        Surname = c.String(),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.BookingCode);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Passenger");
            DropTable("dbo.FlightDetails");
            DropTable("dbo.Flight");
            DropTable("dbo.Booking");
        }
    }
}
