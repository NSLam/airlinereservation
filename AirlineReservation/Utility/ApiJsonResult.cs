﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirlineReservation.Utility
{
    public class ApiJsonResult
    {
        public bool Success { get; set; }

        public object Data { get; set; }
    }
}