﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AirlineReservation.DTO
{
    public class BookingDto
    {
       
        public string BookingCode { get; set; }

        public string TimeReservation { get; set; }

        public decimal ? TotalAmount { get; set; }

        public int Status { get; set; }
    }
}