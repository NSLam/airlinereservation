﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirlineReservation.DTO
{
    public class FlightDetailsDto
    {
        public string BookingCode { get; set; }

        public string FlightCode { get; set; }

        public string Date { get; set; }

        public string Class { get; set; }

        public string Cost { get; set; }
    }
}