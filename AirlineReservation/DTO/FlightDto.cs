﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AirlineReservation.DTO
{
    public class FlightDto
    {
        public string FlightCode { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public string Date { get; set; }

        public string Time { get; set; }

        public string Class { get; set; }

        public string Cost { get; set; }

        public int NumberOfSeats { get; set; }

        public decimal ? Price { get; set; }

    }
}