﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirlineReservation.DTO
{
    public class PassengerDto
    {
        public string BookingCode { get; set; }

        public string Nickname { get; set; }

        public string Surname { get; set; }

        public string Name { get; set; }
    }
}