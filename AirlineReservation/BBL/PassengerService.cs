﻿using AirlineReservation.DAL;
using AirlineReservation.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AirlineReservation.DTO;

namespace AirlineReservation.BBL
{
    public class PassengerService
    {
        public IList<PassengerDto> GetListPassenger()
        {
            var p = PassengerDAL.GetListPassenger();
            return p;
        }

        public string AddPassenger(PassengerReq passengerReq)
        {
            return PassengerDAL.AddPassenger(passengerReq);
        }

        public PassengerDto PassengerDetails(PassengerReq passengerReq)
        {
            return PassengerDAL.PassengerDetails(passengerReq);
        }
    }
}
