﻿using AirlineReservation.DAL;
using AirlineReservation.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AirlineReservation.DTO;

namespace AirlineReservation.BBL
{
    public class BookingService
    {
        public IList<BookingDto> GetListBooking()
        {
            var b = BookingDAL.GetListBooking();
            return b;
        }
        public string AddBooking(string bookingCode, FlightReq flightReq)
        {
            return BookingDAL.AddBooking(bookingCode, flightReq);
        }

        public string AddBookingReturn(string bookingCode, FlightReq flightReq)
        {
            return BookingDAL.AddBookingReturn(bookingCode, flightReq);
        }

        public BookingDto ConfirmBooking(BookingReq bookingReq)
        {
            return BookingDAL.ConfirmBooking(bookingReq);
        }

        public string PaymentBooking(BookingDto bookingReq)
        {
            return BookingDAL.PaymentBooking(bookingReq);
        }
    }
}
