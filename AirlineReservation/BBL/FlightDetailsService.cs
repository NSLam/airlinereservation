﻿using AirlineReservation.DAL;
using AirlineReservation.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AirlineReservation.DTO;

namespace AirlineReservation.BBL
{
    public class FlightDetailsService
    {
        public IList<FlightDetailsDto> GetListFlightDetails()
        {
            var fD = FlightDetailsDAL.GetListFlightDetails();
            return fD;
        }

        public string AddFlightDetails(FlightReq flightReq)
        {
            return FlightDetailsDAL.AddFlightDetails(flightReq);
        }

        public string AddFlightDetailsReturn(FlightReq flightReq)
        {
            return FlightDetailsDAL.AddFlightDetailsReturn(flightReq);
        }
    }
}
