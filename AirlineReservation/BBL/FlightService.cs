﻿using AirlineReservation.DAL;
using AirlineReservation.DAL.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AirlineReservation.DTO;

namespace AirlineReservation.BBL
{
    public class FlightService
    {
        public IList<FlightDto> Search(FlightReq flightReq)
        {
            var f = FlightDAL.Search(flightReq);
            return f;
        }

        public IList<FlightDto> GetFlyingFrom()
        {
            var fF = FlightDAL.GetFlyingFrom();
            return fF;
        }

        public IList<FlightDto> GetFlyingTo(string flyingFrom)
        {
            var fT = FlightDAL.GetFlyingTo(flyingFrom);
            return fT;
        }

        public IList<FlightDto> GetFlyingClass()
        {
            var fCl = FlightDAL.GetFlyingClass();
            return fCl;
        }

        public IList<FlightDto> GetFlyingCost()
        {
            var fCo = FlightDAL.GetFlyingCost();
            return fCo;
        }

        public FlightDto ConfirmBooking(FlightReq flightReq)
        {
            return FlightDAL.ConfirmBooking(flightReq);
        }

        public FlightDto ConfirmReturnStep1(FlightReq flightReq)
        {
            return FlightDAL.ConfirmReturnStep1(flightReq);
        }

        public IList<FlightDto> GetListFlight()
        {
            var f = FlightDAL.GetListFlight();
            return f;
        }

        public string AddOrUpdate(FlightDto vm)
        {
            return FlightDAL.AddOrUpdate(vm);
        }

        public FlightDto Details(FlightReq vm)
        {
            return FlightDAL.Details(vm);
        }

        public IList<FlightDto> SearchReturn(FlightReq flightReq)
        {
            var fR = FlightDAL.SearchReturn(flightReq);
            return fR;
        }
    }
}