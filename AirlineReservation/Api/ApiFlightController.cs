﻿using System;
using System.Web.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using AirlineReservation.BBL;
using AirlineReservation.DAL;
using AirlineReservation.Utility;
using AirlineReservation.DAL.Models;
using AirlineReservation.DTO;

namespace VNPOST.Web.Frontend.Api
{
    [RoutePrefix("api/Flight")]
    public class ApiFlightController : ApiController
    {
        private readonly FlightService _flightService;

        public ApiFlightController() 
        {
            _flightService = new FlightService();
        }

        [Route("Search")]
        [HttpPost]
        public ApiJsonResult Search(FlightReq flightReq)
        {
            try
            {
                IList<FlightDto> flight = _flightService.Search(flightReq);
                return new ApiJsonResult { Success = true, Data = flight };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }

        [Route("GetFlyingFrom")]
        [HttpGet]
        public ApiJsonResult GetFlyingFrom()
        {
            try
            {
                IList<FlightDto> flyingFrom = _flightService.GetFlyingFrom();
                return new ApiJsonResult { Success = true, Data = flyingFrom };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }

        [Route("GetFlyingTo")]
        [HttpGet]
        public ApiJsonResult GetFlyingTo(string flyingFrom)
        {
            try
            {
                IList<FlightDto> flyingTo = _flightService.GetFlyingTo(flyingFrom);
                return new ApiJsonResult { Success = true, Data = flyingTo };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }

        [Route("GetFlyingClass")]
        [HttpGet]
        public ApiJsonResult GetFlyingClass()
        {
            try
            {
                IList<FlightDto> flyingClass = _flightService.GetFlyingClass();
                return new ApiJsonResult { Success = true, Data = flyingClass };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }

        [Route("GetFlyingCost")]
        [HttpGet]
        public ApiJsonResult GetFlyingCost()
        {
            try
            {
                IList<FlightDto> flyingCost = _flightService.GetFlyingCost();
                return new ApiJsonResult { Success = true, Data = flyingCost };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }

        [Route("Confirm")]
        [HttpPost]
        public ApiJsonResult ConfirmBooking(FlightReq flightReq)
        {
            try
            {
                var mRet = new FlightDto();
                mRet = _flightService.ConfirmBooking(flightReq);
                return new ApiJsonResult { Success = true, Data = mRet };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }

        [Route("ConfirmReturnStep1")]
        [HttpPost]
        public ApiJsonResult ConfirmReturnStep1(FlightReq flightReq)
        {
            try
            {
                var mRet = new FlightDto();
                mRet = _flightService.ConfirmReturnStep1(flightReq);
                return new ApiJsonResult { Success = true, Data = mRet };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }

        [Route("GetListFlight")]
        [HttpGet]
        public ApiJsonResult GetListFlight()
        {
            try
            {
                IList<FlightDto> flight = _flightService.GetListFlight();
                return new ApiJsonResult { Success = true, Data = flight };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }

        [Route("AddOrUpdate")]
        [HttpPost]
        public async Task<ApiJsonResult> AddOrUpdate(FlightDto vm)
        {
            try
            {
                var mRet = _flightService.AddOrUpdate(vm);
                return new ApiJsonResult { Success = true, Data = mRet };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }

        [Route("Details")]
        [HttpPost]
        public ApiJsonResult Details(FlightReq vm)
        {
            try
            {
                var mRet = new FlightDto();
                mRet = _flightService.Details(vm);
                return new ApiJsonResult { Success = true, Data = mRet };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }

        [Route("SearchReturn")]
        [HttpPost]
        public ApiJsonResult SearchReturn(FlightReq flightReq)
        {
            try
            {
                IList<FlightDto> flightReturn = _flightService.SearchReturn(flightReq);
                return new ApiJsonResult { Success = true, Data = flightReturn };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }

        protected ApiJsonResult ProcessException(Exception ex)
        {
            return new ApiJsonResult { Success = false, Data = ApiErrorCode.SYSTEM_ERROR + "=>" + ex.Message };
        }

    }
}
