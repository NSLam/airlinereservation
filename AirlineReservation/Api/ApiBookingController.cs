﻿using System;
using System.Web.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using AirlineReservation.BBL;
using AirlineReservation.DAL;
using AirlineReservation.Utility;
using AirlineReservation.DAL.Models;
using AirlineReservation.DTO;

namespace VNPOST.Web.Frontend.Api
{
    [RoutePrefix("api/Booking")]
    public class ApiBookingController : ApiController
    {
        private readonly BookingService _bookingService;

        public ApiBookingController() 
        {
            _bookingService = new BookingService();
        }

        [Route("GetListBooking")]
        [HttpPost]
        public ApiJsonResult GetListBooking()
        {
            try
            {
                IList<BookingDto> booking = _bookingService.GetListBooking();
                return new ApiJsonResult { Success = true, Data = booking };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }

        [Route("ConfirmBooking")]
        [HttpPost]
        public ApiJsonResult ConfirmBooking(BookingReq bookingReq)
        {
            try
            {
                var mRet = new BookingDto();
                mRet = _bookingService.ConfirmBooking(bookingReq);
                return new ApiJsonResult { Success = true, Data = mRet };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }

        [Route("PaymentBooking")]
        [HttpPost]
        public async Task<ApiJsonResult> PaymentBooking(BookingDto bookingReq)
        {
            try
            {
                var bRet = _bookingService.PaymentBooking(bookingReq);
                return new ApiJsonResult { Success = true, Data = bRet };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }


        protected ApiJsonResult ProcessException(Exception ex)
        {
            return new ApiJsonResult { Success = false, Data = ApiErrorCode.SYSTEM_ERROR + "=>" + ex.Message };
        }

    }
}
