﻿using System;
using System.Web.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using AirlineReservation.BBL;
using AirlineReservation.DAL;
using AirlineReservation.Utility;
using AirlineReservation.DAL.Models;
using AirlineReservation.DTO;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace VNPOST.Web.Frontend.Api
{
    [RoutePrefix("api/Passenger")]
    public class ApiPassengerController : ApiController
    {
        private readonly PassengerService _passengerService;

        public ApiPassengerController() 
        {
            _passengerService = new PassengerService();
        }


        [Route("AddPassenger")]
        [HttpPost]
        public async Task<ApiJsonResult> AddPassenger(PassengerReq passengerReq)
        {
            try
            {
                var pRet = _passengerService.AddPassenger(passengerReq);
                return new ApiJsonResult { Success = true, Data = pRet };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }

        }

        [Route("PassengerDetails")]
        [HttpPost]
        public ApiJsonResult PassengerDetails(PassengerReq passengerReq)
        {
            try
            {
                var pRet = _passengerService.PassengerDetails(passengerReq);
                return new ApiJsonResult { Success = true, Data = pRet };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }

        }

        [Route("GetListPassenger")]
        [HttpPost]
        public ApiJsonResult GetListPassenger()
        {
            try
            {
                IList<PassengerDto> passenger = _passengerService.GetListPassenger();
                return new ApiJsonResult { Success = true, Data = passenger };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }

        protected ApiJsonResult ProcessException(Exception ex)
        {
            return new ApiJsonResult { Success = false, Data = ApiErrorCode.SYSTEM_ERROR + "=>" + ex.Message };
        }

    }
}
