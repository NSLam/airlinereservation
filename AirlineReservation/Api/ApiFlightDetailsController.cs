﻿using System;
using System.Web.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using AirlineReservation.BBL;
using AirlineReservation.DAL;
using AirlineReservation.Utility;
using AirlineReservation.DAL.Models;
using AirlineReservation.DTO;
using System.Data.Entity.Validation;
using System.Diagnostics;

namespace VNPOST.Web.Frontend.Api
{
    [RoutePrefix("api/FlightDetails")]
    public class ApiFlightDetailsController : ApiController
    {
        private readonly FlightDetailsService _flightDetailsService;
        private readonly BookingService _bookingService;

        public ApiFlightDetailsController() 
        {
            _flightDetailsService = new FlightDetailsService();
            _bookingService = new BookingService();
        }


        [Route("AddFlightDetails")]
        [HttpPost]
        public async Task<ApiJsonResult> AddFlightDetails(FlightReq flightReq)
        {
            try
            {
                var mRet = _flightDetailsService.AddFlightDetails(flightReq);
                var bRet = _bookingService.AddBooking(mRet, flightReq);
                return new ApiJsonResult { Success = true, Data = mRet };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }

        }

        [Route("AddFlightDetailsReturn")]
        [HttpPost]
        public async Task<ApiJsonResult> AddFlightDetailsReturn(FlightReq flightReq)
        {
            try
            {
                var mRet = _flightDetailsService.AddFlightDetailsReturn(flightReq);
                var bRet = _bookingService.AddBookingReturn(mRet, flightReq);
                return new ApiJsonResult { Success = true, Data = mRet };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }

        }

        [Route("GetListFlightDetails")]
        [HttpPost]
        public ApiJsonResult GetListFlightDetails()
        {
            try
            {
                IList<FlightDetailsDto> flightDetails = _flightDetailsService.GetListFlightDetails();
                return new ApiJsonResult { Success = true, Data = flightDetails };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }

        protected ApiJsonResult ProcessException(Exception ex)
        {
            return new ApiJsonResult { Success = false, Data = ApiErrorCode.SYSTEM_ERROR + "=>" + ex.Message };
        }

    }
}
