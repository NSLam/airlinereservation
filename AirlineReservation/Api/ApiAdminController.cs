﻿using System;
using System.Web.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using AirlineReservation.BBL;
using AirlineReservation.DAL;
using AirlineReservation.Utility;
using AirlineReservation.DAL.Models;
using AirlineReservation.DTO;

namespace VNPOST.Web.Frontend.Api
{
    [Authorize]
    [RoutePrefix("api/Admin")]
    public class ApiAdminController : ApiController
    {
        private readonly FlightService _flightService;

        public ApiAdminController() 
        {
            _flightService = new FlightService();
        }

        [Route("GetListFlight")]
        [HttpPost]
        public ApiJsonResult GetListFlight()
        {
            try
            {
                IList<FlightDto> flight = _flightService.GetListFlight();
                return new ApiJsonResult { Success = true, Data = flight };
            }
            catch (Exception ex)
            {
                return ProcessException(ex);
            }
        }

        //[Route("GetFlyingFrom")]
        //[HttpGet]
        //public ApiJsonResult GetFlyingFrom()
        //{
        //    try
        //    {
        //        IList<FlightDto> flyingFrom = _flightService.GetFlyingFrom();
        //        return new ApiJsonResult { Success = true, Data = flyingFrom };
        //    }
        //    catch (Exception ex)
        //    {
        //        return ProcessException(ex);
        //    }
        //}

        //[Route("GetFlyingTo")]
        //[HttpGet]
        //public ApiJsonResult GetFlyingTo(string flyingFrom)
        //{
        //    try
        //    {
        //        IList<FlightDto> flyingTo = _flightService.GetFlyingTo(flyingFrom);
        //        return new ApiJsonResult { Success = true, Data = flyingTo };
        //    }
        //    catch (Exception ex)
        //    {
        //        return ProcessException(ex);
        //    }
        //}

        //[Route("GetFlyingClass")]
        //[HttpGet]
        //public ApiJsonResult GetFlyingClass()
        //{
        //    try
        //    {
        //        IList<FlightDto> flyingClass = _flightService.GetFlyingClass();
        //        return new ApiJsonResult { Success = true, Data = flyingClass };
        //    }
        //    catch (Exception ex)
        //    {
        //        return ProcessException(ex);
        //    }
        //}

        //[Route("GetFlyingCost")]
        //[HttpGet]
        //public ApiJsonResult GetFlyingCost()
        //{
        //    try
        //    {
        //        IList<FlightDto> flyingCost = _flightService.GetFlyingCost();
        //        return new ApiJsonResult { Success = true, Data = flyingCost };
        //    }
        //    catch (Exception ex)
        //    {
        //        return ProcessException(ex);
        //    }
        //}

        //[Route("Confirm")]
        //[HttpPost]
        //public ApiJsonResult ConfirmBooking(FlightReq flightReq)
        //{
        //    try
        //    {
        //        var mRet = new FlightDto();
        //        mRet = _flightService.ConfirmBooking(flightReq);
        //        return new ApiJsonResult { Success = true, Data = mRet };
        //    }
        //    catch (Exception ex)
        //    {
        //        return ProcessException(ex);
        //    }
        //}

        protected ApiJsonResult ProcessException(Exception ex)
        {
            return new ApiJsonResult { Success = false, Data = ApiErrorCode.SYSTEM_ERROR + "=>" + ex.Message };
        }

    }
}
