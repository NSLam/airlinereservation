﻿using AirlineReservation.DAL.Models;
using System;
using System.Web.Mvc;

namespace AirlineReservation.Controllers
{
    [RoutePrefix("flight")]
    public class FlightController : Controller
    {
        [Route("index")]
        public ActionResult Index()
        {
            
            return View();
        }

        //[Route("details/{flyingFrom}/{flyingTo}/{flyingDate}/{flyingClass}/{flyingCost}")]
        //public ActionResult Details(string flyingFrom, string flyingTo, string flyingDate, string flyingClass, string flyingCost)
        //{
        //    var fYear = int.Parse(flyingDate.Substring(4, 4));
        //    var fMonth = int.Parse(flyingDate.Substring(2, 2));
        //    var fDay = int.Parse(flyingDate.Substring(0, 2));
        //    var fDate = new DateTime(fYear, fMonth, fDay);
        //    ViewData["FLYINGFROM"] = flyingFrom;
        //    ViewData["FLYINGTO"] = flyingTo;
        //    ViewData["FLYINGDATE"] = fDate.ToString("dd/MM/yyyy");
        //    ViewData["FLYINGDATEDAY"] = fDay;
        //    ViewData["FLYINGDATEMONTH"] = fMonth;
        //    ViewData["FLYINGDATEYEAR"] = fYear;
        //    ViewData["FLYINGCLASS"] = flyingClass;
        //    ViewData["FLYINGCOST"] = flyingCost;
        //    return View();
        //}

        //[Route("confirm/{flyingFrom}/{flyingTo}/{flyingDate}/{flyingClass}/{flyingCost}")]
        //public ActionResult Confirm(string flyingFrom, string flyingTo, string flyingDate, string flyingClass, string flyingCost)
        //{
        //    var fYear = int.Parse(flyingDate.Substring(4, 4));
        //    var fMonth = int.Parse(flyingDate.Substring(2, 2));
        //    var fDay = int.Parse(flyingDate.Substring(0, 2));
        //    var fDate = new DateTime(fYear, fMonth, fDay);
        //    ViewData["FLYINGFROM"] = flyingFrom;
        //    ViewData["FLYINGTO"] = flyingTo;
        //    ViewData["FLYINGDATE"] = fDate.ToString("dd/MM/yyyy");
        //    ViewData["FLYINGCLASS"] = flyingClass;
        //    ViewData["FLYINGCOST"] = flyingCost;
        //    return View();
        //}

        //[Route("passenger/{flyingFrom}")]
        //public ActionResult Passenger(string flyingFrom)
        //{
        //    ViewData["BOOKINGCODE"] = flyingFrom;

        //    return View();
        //}
    }


}