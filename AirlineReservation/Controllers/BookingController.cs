﻿using AirlineReservation.DAL.Models;
using System;
using System.Web.Mvc;

namespace AirlineReservation.Controllers
{
    [RoutePrefix("booking")]
    public class BookingController : Controller
    {
        [Route("index")]
        public ActionResult Index()
        {            
            return View();
        }

        [Route("payment/{flyingFrom}")]
        public ActionResult Payment(string flyingFrom)
        {
            ViewData["FLYINGFROM"] = flyingFrom;

            return View();
        }        
    }


}