﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AirlineReservation.Controllers
{
    [AllowAnonymous]
    [RoutePrefix("login")]
    public class LoginController : Controller
    {
        // GET: Login
        [Route("securityinfo")]
        public ActionResult SecurityInfo()
        {
            return View();
        }
    }
}