﻿using AirlineReservation.DAL.Models;
using System;
using System.Web.Mvc;

namespace AirlineReservation.Controllers
{
    [RoutePrefix("airline")]
    public class AirlineController : Controller
    {
        [Route("index")]
        public ActionResult Index()
        {
            
            return View();
        }

        [Route("details/{flyingFrom}/{flyingTo}/{flyingDate}/{flyingClass}/{flyingCost}")]
        public ActionResult Details(string flyingFrom, string flyingTo, string flyingDate, string flyingClass, string flyingCost)
        {
            var fYear = int.Parse(flyingDate.Substring(4, 4));
            var fMonth = int.Parse(flyingDate.Substring(2, 2));
            var fDay = int.Parse(flyingDate.Substring(0, 2));
            var fDate = new DateTime(fYear, fMonth, fDay);
            ViewData["FLYINGFROM"] = flyingFrom;
            ViewData["FLYINGTO"] = flyingTo;
            ViewData["FLYINGDATE"] = fDate.ToString("dd/MM/yyyy");
            ViewData["FLYINGDATEDAY"] = fDay;
            ViewData["FLYINGDATEMONTH"] = fMonth;
            ViewData["FLYINGDATEYEAR"] = fYear;
            ViewData["FLYINGCLASS"] = flyingClass;
            ViewData["FLYINGCOST"] = flyingCost;
            return View();
        }

        [Route("confirm/{flyingFrom}/{flyingTo}/{flyingDate}/{flyingClass}/{flyingCost}/{flyingDateReturn}")]
        public ActionResult Confirm(string flyingFrom, string flyingTo, string flyingDate, string flyingClass, string flyingCost, string flyingDateReturn)
        {
            var fYear = int.Parse(flyingDate.Substring(4, 4));
            var fMonth = int.Parse(flyingDate.Substring(2, 2));
            var fDay = int.Parse(flyingDate.Substring(0, 2));
            var fDate = new DateTime(fYear, fMonth, fDay);
            ViewData["FLYINGFROM"] = flyingFrom;
            ViewData["FLYINGTO"] = flyingTo;
            ViewData["FLYINGDATE"] = fDate.ToString("dd/MM/yyyy");
            ViewData["FLYINGCLASS"] = flyingClass;
            ViewData["FLYINGCOST"] = flyingCost;
            ViewData["FLYINGDATERETURN"] = flyingDateReturn;
            return View();
        }

        [Route("passenger/{flyingFrom}")]
        public ActionResult Passenger(string flyingFrom)
        {
            ViewData["BOOKINGCODE"] = flyingFrom;

            return View();
        }

        [Route("passenger/{flyingFrom}")]
        public ActionResult PassengerReturn(string flyingFrom)
        {
            ViewData["BOOKINGCODE"] = flyingFrom;

            return View();
        }

        [Route("indexreturn")]
        public ActionResult IndexReturn()
        {

            return View();
        }

        [Route("detailsreturn/{flyingFrom}/{flyingTo}/{flyingDate}/{flyingClass}/{flyingCost}/{flyingDateReturn}")]
        public ActionResult DetailsReturn(string flyingFrom, string flyingTo, string flyingDate, string flyingClass, string flyingCost, string flyingDateReturn)
        {
            var fYear = int.Parse(flyingDate.Substring(4, 4));
            var fMonth = int.Parse(flyingDate.Substring(2, 2));
            var fDay = int.Parse(flyingDate.Substring(0, 2));
            var fDate = new DateTime(fYear, fMonth, fDay);
            ViewData["FLYINGFROM"] = flyingFrom;
            ViewData["FLYINGTO"] = flyingTo;
            ViewData["FLYINGDATE"] = fDate.ToString("dd/MM/yyyy");
            ViewData["FLYINGDATEDAY"] = fDay;
            ViewData["FLYINGDATEMONTH"] = fMonth;
            ViewData["FLYINGDATEYEAR"] = fYear;
            ViewData["FLYINGCLASS"] = flyingClass;
            ViewData["FLYINGCOST"] = flyingCost;
            var fYearReturn = int.Parse(flyingDateReturn.Substring(4, 4));
            var fMonthReturn = int.Parse(flyingDateReturn.Substring(2, 2));
            var fDayReturn = int.Parse(flyingDateReturn.Substring(0, 2));
            var fDateReturn = new DateTime(fYearReturn, fMonthReturn, fDayReturn);
            ViewData["FLYINGDATERETURN"] = fDateReturn.ToString("dd/MM/yyyy");
            return View();
        }

        [Route("confirmreturnstep1/{flyingFrom}/{flyingTo}/{flyingDate}/{flyingClass}/{flyingCost}/{flyingDateReturn}/{flightCode}")]
        public ActionResult ConfirmReturnStep1(string flyingFrom, string flyingTo, string flyingDate, string flyingClass, string flyingCost, string flyingDateReturn, string flightCode)
        {
            var fYear = int.Parse(flyingDate.Substring(4, 4));
            var fMonth = int.Parse(flyingDate.Substring(2, 2));
            var fDay = int.Parse(flyingDate.Substring(0, 2));
            var fDate = new DateTime(fYear, fMonth, fDay);
            ViewData["FLYINGFROM"] = flyingFrom;
            ViewData["FLYINGTO"] = flyingTo;
            ViewData["FLYINGDATE"] = fDate.ToString("dd/MM/yyyy");
            ViewData["FLYINGCLASS"] = flyingClass;
            ViewData["FLYINGCOST"] = flyingCost;
            var fYearReturn = int.Parse(flyingDateReturn.Substring(4, 4));
            var fMonthReturn = int.Parse(flyingDateReturn.Substring(2, 2));
            var fDayReturn = int.Parse(flyingDateReturn.Substring(0, 2));
            var fDateReturn = new DateTime(fYearReturn, fMonthReturn, fDayReturn);
            ViewData["FLYINGDATERETURN"] = fDateReturn.ToString("dd/MM/yyyy");
            ViewData["FLIGHTCODE"] = flightCode;
            return View();
        }

        [Route("confirmreturnstep2/{flyingFrom}/{flyingTo}/{flyingDate}/{flyingClass}/{flyingCost}/{flyingDateReturn}/{flightCode}")]
        public ActionResult ConfirmReturnStep2(string flyingFrom, string flyingTo, string flyingDate, string flyingClass, string flyingCost, string flyingDateReturn, string flightCode)
        {
            var fYear = int.Parse(flyingDate.Substring(4, 4));
            var fMonth = int.Parse(flyingDate.Substring(2, 2));
            var fDay = int.Parse(flyingDate.Substring(0, 2));
            var fDate = new DateTime(fYear, fMonth, fDay);
            ViewData["FLYINGFROM"] = flyingFrom;
            ViewData["FLYINGTO"] = flyingTo;
            ViewData["FLYINGDATE"] = fDate.ToString("dd/MM/yyyy");
            ViewData["FLYINGCLASS"] = flyingClass;
            ViewData["FLYINGCOST"] = flyingCost;
            ViewData["FLYINGDATERETURN"] = flyingDateReturn;
            ViewData["FLIGHTCODE"] = flightCode;
            return View();
        }
    }


}