﻿using AirlineReservation.DAL.Models;
using AirlineReservation.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using System.Web;

namespace AirlineReservation.DAL
{
    public static class PassengerDAL
    {
        private static readonly AirlineReservationContext _db = new AirlineReservationContext();

        public static IList<PassengerDto> GetListPassenger()
        {
            var passengerList = new List<PassengerDto>();

            var passenger = _db.Passenger.Select(x => x);

            foreach (var i in passenger.ToList())
            {
                var p = new PassengerDto
                {
                    BookingCode = i.BookingCode,
                    Nickname = i.Nickname,
                    Surname = i.Surname,
                    Name = i.Name
                };

                passengerList.Add(p);
            }

            return passengerList;
        }

        public static string AddPassenger(PassengerReq passengerReq)
        {
            var p = new Passenger()
            {
                BookingCode = passengerReq.BookingCode,
                Nickname = passengerReq.Nickname,
                Surname = passengerReq.Surname,
                Name = passengerReq.Name,
            };
            _db.Passenger.Add(p);
            _db.SaveChanges();

            return p.BookingCode;
        }

        public static PassengerDto PassengerDetails(PassengerReq passengerReq)
        {            
            var mRet = new PassengerDto()
            {
                BookingCode = passengerReq.BookingCode,
                Nickname = passengerReq.Nickname,
                Surname = passengerReq.Surname,
                Name = passengerReq.Name,
            };
            return mRet;
        }
    }
}