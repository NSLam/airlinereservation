﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AirlineReservation.DAL.Models
{
    public class Flight
    {
        
        public string FlightCode { get; set; }

        public string From { get; set; }

        public string To { get; set; }

        public DateTime Date { get; set; }

        public DateTime Time { get; set; }

        public string Class { get; set; }

        public string Cost { get; set; }

        public int NumberOfSeats { get; set; }

        public decimal ? Price { get; set; }
    }
}