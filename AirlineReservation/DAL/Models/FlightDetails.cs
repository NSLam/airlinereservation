﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AirlineReservation.DAL.Models
{
    public class FlightDetails
    {
        public string BookingCode { get; set; }

        public string FlightCode { get; set; }

        public DateTime Date { get; set; }

        public string Class { get; set; }

        public string Cost { get; set; }
    }
}