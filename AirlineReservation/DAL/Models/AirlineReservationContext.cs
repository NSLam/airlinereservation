﻿using AirlineReservation.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace AirlineReservation.DAL.Models
{
    public class AirlineReservationContext : DbContext
    {
        public AirlineReservationContext() : base("AirlineReservationContext")
        {
        }

        public DbSet<Flight> Flight { get; set; }
        public DbSet<Booking> Booking { get; set; }
        public DbSet<FlightDetails> FlightDetails { get; set; }
        public DbSet<Passenger> Passenger { get; set; }

        //public DbSet<ExternalLoginViewModel> ExternalLoginViewModel { get; set; }
        //public DbSet<ManageInfoViewModel> ManageInfoViewModel { get; set; }
        //public DbSet<UserInfoViewModel> UserInfoViewModel { get; set; }
        //public DbSet<UserLoginInfoViewModel> UserLoginInfoViewModel { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<Flight>().HasKey(t => new { t.FlightCode, t.From, t.To, t.Date, t.Class, t.Cost });
            modelBuilder.Entity<Booking>().HasKey(t => new { t.BookingCode });
            modelBuilder.Entity<FlightDetails>().HasKey(t => new { t.FlightCode, t.BookingCode, t.Date });
            modelBuilder.Entity<Passenger>().HasKey(t => new { t.BookingCode });
        }
    }
}