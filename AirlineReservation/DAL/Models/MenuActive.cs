﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirlineReservation.DAL.Models
{
    public class MenuActive
    {
        public string ActiveName { get; set; }
        public string ExpandName { get; set; }
    }
}