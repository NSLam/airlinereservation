﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AirlineReservation.DAL.Models
{
    public class Booking
    {
        
        public string BookingCode { get; set; }

        public DateTime TimeReservation { get; set; }

        public decimal ? TotalAmount { get; set; }

        public int Status { get; set; }
    }
}