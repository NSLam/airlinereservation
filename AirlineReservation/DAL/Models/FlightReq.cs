﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace AirlineReservation.DAL.Models
{
    public class FlightReq
    {
        
        public string FlightCode { get; set; }

        public string flyingFrom { get; set; }

        public string flyingTo { get; set; }

        public string flyingDate { get; set; }

        public DateTime? flyingTime { get; set; }

        public string flyingClass { get; set; }

        public string flyingCost { get; set; }

        public decimal flyingPrice { get; set; }

        public string flyingDateReturn { get; set; }
    }
}