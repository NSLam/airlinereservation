﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirlineReservation.DAL.Models
{
    public enum ApiErrorCode
    {
        ACCOUNT_IS_LOCKED,
        ACCOUNT_NOT_EXISTED,
        CAN_NOT_ADD_FAVORITE_WITH_YOURSELF,
        CAN_NOT_RATE_YOURSELF,
        CURRENT_PASSWORD_INCORRECT,
        EMAIL_IN_USED,
        EMAIL_INVALID,
        INVALID,
        INVALID_SESSION,
        NO_PERMISSION,
        SYSTEM_ERROR,
        NOT_FOUND,
        CLAIM_OUT_DATE,
        CLAIM_OVER,
        USER_INVALID,
        PRICE_NOT_FOUND,
        USER_INVALID_ROLE,
        EXISTED
    }
}