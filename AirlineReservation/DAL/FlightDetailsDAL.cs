﻿using AirlineReservation.DAL.Models;
using AirlineReservation.DTO;
using System;
using System.Collections.Generic;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using System.Web;

namespace AirlineReservation.DAL
{
    public static class FlightDetailsDAL
    {
        private static readonly AirlineReservationContext _db = new AirlineReservationContext();

        public static IList<FlightDetailsDto> GetListFlightDetails()
        {
            var flightDetailsList = new List<FlightDetailsDto>();

            var flightDetails = _db.FlightDetails.Select(x => x);

            foreach (var i in flightDetails.ToList())
            {
                var fD = new FlightDetailsDto
                {
                    BookingCode = i.BookingCode,
                    FlightCode = i.FlightCode,
                    Date = i.Date.ToString("dd/MM/yyyy"),
                    Class = i.Class,
                    Cost = i.Cost
                };

                flightDetailsList.Add(fD);
            }

            return flightDetailsList;
        }

        public static string AddFlightDetails(FlightReq flightReq)
        {
            var datetoEnter = DateTime.ParseExact(flightReq.flyingDate, "yyyy-dd-MM", CultureInfo.InvariantCulture);

            var fD = new FlightDetails()
            {
                BookingCode = RandomString(6),
                FlightCode = flightReq.FlightCode,
                Date = datetoEnter,
                Class = flightReq.flyingClass,
                Cost = flightReq.flyingCost
            };
            _db.FlightDetails.Add(fD);
            _db.SaveChanges();

            var datetoEnterFlight = DateTime.ParseExact(flightReq.flyingDate, "yyyy-dd-MM", CultureInfo.InvariantCulture);
            var flight = _db.Flight.FirstOrDefault(x => x.FlightCode == flightReq.FlightCode &&
                                              x.From == flightReq.flyingFrom &&
                                              x.To == flightReq.flyingTo &&
                                              x.Date == datetoEnterFlight &&
                                              x.Class == flightReq.flyingClass &&
                                              x.Cost == flightReq.flyingCost);

            flight.NumberOfSeats = flight.NumberOfSeats - 1;

            _db.SaveChanges();

            return fD.BookingCode;
        }

        public static string AddFlightDetailsReturn(FlightReq flightReq)
        {
            var datetoEnter = DateTime.ParseExact(flightReq.flyingDate, "yyyy-dd-MM", CultureInfo.InvariantCulture);

            var fD = new FlightDetails()
            {
                BookingCode = flightReq.flyingDateReturn,
                FlightCode = flightReq.FlightCode,
                Date = datetoEnter,
                Class = flightReq.flyingClass,
                Cost = flightReq.flyingCost
            };
            _db.FlightDetails.Add(fD);
            _db.SaveChanges();

            var datetoEnterFlight = DateTime.ParseExact(flightReq.flyingDate, "yyyy-dd-MM", CultureInfo.InvariantCulture);
            var flight = _db.Flight.FirstOrDefault(x => x.FlightCode == flightReq.FlightCode &&
                                              x.From == flightReq.flyingFrom &&
                                              x.To == flightReq.flyingTo &&
                                              x.Date == datetoEnterFlight &&
                                              x.Class == flightReq.flyingClass &&
                                              x.Cost == flightReq.flyingCost);

            flight.NumberOfSeats = flight.NumberOfSeats - 1;

            _db.SaveChanges();

            return fD.BookingCode;
        }       

        private static Random random = new Random();
        public static string RandomString(int length)
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}