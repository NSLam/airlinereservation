﻿using AirlineReservation.DAL.Models;
using AirlineReservation.DTO;
using Microsoft.Ajax.Utilities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Globalization;
using System.Linq;
using System.Web;

namespace AirlineReservation.DAL
{
    public static class FlightDAL
    {
        private static readonly AirlineReservationContext _db = new AirlineReservationContext();

        public static IList<FlightDto> Search(FlightReq flightReq)
        {
            var flightList = new List<FlightDto>();

            var flight = _db.Flight.Select(x => new
            {
                FlightCode = x.FlightCode,
                From = x.From,
                To = x.To,
                Date = x.Date,
                Time = x.Time,
                Class = x.Class,
                Cost = x.Cost,
                NumberOfSeats = x.NumberOfSeats,
                Price = x.Price
            });

            var datetoEnter = DateTime.ParseExact(flightReq.flyingDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            if (!string.IsNullOrEmpty(flightReq.flyingFrom))
                flight = flight.Where(p => p.From == flightReq.flyingFrom);
            if (!string.IsNullOrEmpty(flightReq.flyingTo))
                flight = flight.Where(p => p.To == flightReq.flyingTo);
            if (!string.IsNullOrEmpty(flightReq.flyingDate))
                flight = flight.Where(p => p.Date == datetoEnter);
            if (!string.IsNullOrEmpty(flightReq.flyingClass))
                flight = flight.Where(p => p.Class == flightReq.flyingClass);
            if (!string.IsNullOrEmpty(flightReq.flyingCost))
                flight = flight.Where(p => p.Cost == flightReq.flyingCost);

            foreach (var i in flight.ToList())
            {
                var f = new FlightDto
                {
                    FlightCode = i.FlightCode,
                    From = i.From,
                    To = i.To,
                    Date = i.Date.ToString("dd/MM/yyyy"),
                    Time = i.Time.ToString("HH:mm"),
                    Class = i.Class,
                    Cost = i.Cost,
                    NumberOfSeats = i.NumberOfSeats,
                    Price = i.Price
                };
                flightList.Add(f);
            }
            return flightList;
        }

        public static IList<FlightDto> GetFlyingFrom()
        {
            var flyingFromList = new List<FlightDto>();

            var flyingFrom = _db.Flight.DistinctBy(x => x.From);

            foreach (var i in flyingFrom.ToList())
            {
                var f = new FlightDto
                {
                    From = i.From,
                    To = i.To,
                };
                flyingFromList.Add(f);
            }
            return flyingFromList.ToList();
        }

        public static IList<FlightDto> GetFlyingTo(string flyingFrom)
        {
            var flyingToList = new List<FlightDto>();

            //var flyingTo = _db.Flight.DistinctBy(x => x.To).Where(x => x.From == flyingFrom);
            var flyingTo = _db.Flight.Where(x => x.From == flyingFrom).DistinctBy(x => x.To);

            foreach (var i in flyingTo.ToList())
            {
                var f = new FlightDto
                {
                    FlightCode = i.FlightCode,
                    To = i.To,
                };
                flyingToList.Add(f);
            }
            return flyingToList;
        }

        public static IList<FlightDto> GetFlyingClass()
        {
            var flyingClassList = new List<FlightDto>();

            var flyingClass = _db.Flight.DistinctBy(x => x.Class);

            foreach (var i in flyingClass.ToList())
            {
                var f = new FlightDto
                {
                    Class = i.Class,
                };
                flyingClassList.Add(f);
            }

            return flyingClassList;
        }

        public static IList<FlightDto> GetFlyingCost()
        {
            var flyingCostList = new List<FlightDto>();

            var flyingCost = _db.Flight.DistinctBy(x => x.Cost);

            foreach (var i in flyingCost.ToList())
            {
                var f = new FlightDto
                {
                    Cost = i.Cost,
                };
                flyingCostList.Add(f);
            }

            return flyingCostList;
        }

        public static FlightDto ConfirmBooking(FlightReq flightReq)
        {
            var datetoEnter = DateTime.ParseExact(flightReq.flyingDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var flight = _db.Flight.FirstOrDefault(x => x.FlightCode == flightReq.flyingDateReturn &&
                                              x.From == flightReq.flyingFrom &&
                                              x.To == flightReq.flyingTo &&
                                              x.Date == datetoEnter &&
                                              x.Class == flightReq.flyingClass &&
                                              x.Cost == flightReq.flyingCost);

            var mRet = new FlightDto()
            {
                FlightCode = flight.FlightCode,
                From = flight.From,
                To = flight.To,
                Date = flight.Date.ToString("dd/MM/yyyy"),
                Time = flight.Time.ToString("HH:mm"),
                Class = flight.Class,
                Cost = flight.Cost,
                NumberOfSeats = flight.NumberOfSeats,
                Price = flight.Price
            };
            return mRet;
        }

        public static IList<FlightDto> GetListFlight()
        {
            var flightList = new List<FlightDto>();

            var flight = _db.Flight.Select(x => x);

            foreach (var i in flight.ToList())
            {
                var f = new FlightDto
                {
                    FlightCode = i.FlightCode,
                    From = i.From,
                    To = i.To,
                    Date = i.Date.ToString("dd/MM/yyyy"),
                    Time = i.Time.ToString("HH:mm"),
                    Class = i.Class,
                    Cost = i.Cost,
                    NumberOfSeats = i.NumberOfSeats,
                    Price = i.Price,
                };

                flightList.Add(f);
            }

            return flightList;
        }

        public static string AddOrUpdate(FlightDto vm)
        {
            //var flightQuery = _db.Flight.Select(x => x);
            //var flight = flightQuery.Where(x => x.FlightCode != vm.FlightCode
            //                                 && x.From != vm.From
            //                                 && x.To != vm.To
            //                                 && x.Class != vm.Class
            //                                 && x.Cost != vm.Cost);

            var flight = _db.Flight.Find(vm.FlightCode);

            var datetoEnter = DateTime.ParseExact(vm.Date, "yyyy-dd-MM", CultureInfo.InvariantCulture);
            var timetoEnter = DateTime.ParseExact(vm.Time, "HH:mm", CultureInfo.InvariantCulture);

            //Add
            if (flight == null)
            {
               
                var c = new Flight()
                {
                    FlightCode = vm.FlightCode,
                    From = vm.From,
                    To = vm.To,
                    Date = datetoEnter,
                    Time = timetoEnter,
                    Class = vm.Class,
                    Cost = vm.Cost,
                    NumberOfSeats = vm.NumberOfSeats,
                    Price = vm.Price,
                };
                _db.Flight.Add(c);
                _db.SaveChanges();

                return c.FlightCode;
            }

            flight.FlightCode = vm.FlightCode;
            flight.From = vm.From;
            flight.To = vm.To;
            flight.Date = datetoEnter;
            flight.Time = timetoEnter;
            flight.Cost = vm.Cost;
            flight.Price = vm.Price;
            _db.SaveChanges();


            return flight.FlightCode;
        }

        public static FlightDto Details(FlightReq vm)
        {
            var flight = _db.Flight.Find(vm.FlightCode);

            var mRet = new FlightDto()
            {
                FlightCode = flight.FlightCode,
                From = flight.From,
                To = flight.To,
                Date = flight.Date.ToString("dd/MM/yyyy"),
                Time = flight.Time.ToString("HH:mm"),
                Class = flight.Class,
                Cost = flight.Cost,
                NumberOfSeats = flight.NumberOfSeats,
                Price = flight.Price,
            };
            return mRet;
        }

        public static IList<FlightDto> SearchReturn(FlightReq flightReq)
        {
            var flightList = new List<FlightDto>();

            //var flight = _db.Flight.Select(x => new
            //{
            //    FlightCode = x.FlightCode,
            //    From = x.From,
            //    To = x.To,
            //    Date = x.Date,
            //    Time = x.Time,
            //    Class = x.Class,
            //    Cost = x.Cost,
            //    NumberOfSeats = x.NumberOfSeats,
            //    Price = x.Price
            //});

            var flightReturn = _db.Flight.Select(x => new
            {
                FlightCode = x.FlightCode,
                From = x.From,
                To = x.To,
                Date = x.Date,
                Time = x.Time,
                Class = x.Class,
                Cost = x.Cost,
                NumberOfSeats = x.NumberOfSeats,
                Price = x.Price
            });

            //var datetoEnter = DateTime.ParseExact(flightReq.flyingDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var datetoEnterReturn = DateTime.ParseExact(flightReq.flyingDateReturn, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            //if (!string.IsNullOrEmpty(flightReq.flyingFrom))
            //flight = flight.Where(p => p.From == flightReq.flyingFrom && p.To == flightReq.flyingTo && p.Date == datetoEnter && p.Class == flightReq.flyingClass && p.Cost == flightReq.flyingCost);
            flightReturn = flightReturn.Where(p => p.From == flightReq.flyingTo && p.To == flightReq.flyingFrom && p.Date == datetoEnterReturn && p.Class == flightReq.flyingClass && p.Cost == flightReq.flyingCost);

            //if (!string.IsNullOrEmpty(flightReq.flyingTo))
            //    flight = flight.Where(p => p.To == flightReq.flyingTo && p.From == flightReq.flyingFrom);

            //if (!string.IsNullOrEmpty(flightReq.flyingDate))
            //    flight = flight.Where(p => p.Date == datetoEnter && p.Date == datetoEnterReturn);

            //if (!string.IsNullOrEmpty(flightReq.flyingClass))
            //    flight = flight.Where(p => p.Class == flightReq.flyingClass);

            //if (!string.IsNullOrEmpty(flightReq.flyingCost))
            //    flight = flight.Where(p => p.Cost == flightReq.flyingCost);

            //foreach (var i in flight.ToList())
            //{
            //    var f = new FlightDto
            //    {
            //        FlightCode = i.FlightCode,
            //        From = i.From,
            //        To = i.To,
            //        Date = i.Date.ToString("dd/MM/yyyy"),
            //        Time = i.Time.ToString("HH:mm"),
            //        Class = i.Class,
            //        Cost = i.Cost,
            //        NumberOfSeats = i.NumberOfSeats,
            //        Price = i.Price
            //    };
            //    flightList.Add(f);
            //}

            foreach (var i in flightReturn.ToList())
            {
                var f = new FlightDto
                {
                    FlightCode = i.FlightCode,
                    From = i.From,
                    To = i.To,
                    Date = i.Date.ToString("dd/MM/yyyy"),
                    Time = i.Time.ToString("HH:mm"),
                    Class = i.Class,
                    Cost = i.Cost,
                    NumberOfSeats = i.NumberOfSeats,
                    Price = i.Price
                };
                flightList.Add(f);
            }
            return flightList;
        }

        public static FlightDto ConfirmReturnStep1(FlightReq flightReq)
        {
            var datetoEnter = DateTime.ParseExact(flightReq.flyingDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            var flight = _db.Flight.FirstOrDefault(x => x.FlightCode == flightReq.FlightCode &&
                                              x.From == flightReq.flyingFrom &&
                                              x.To == flightReq.flyingTo &&
                                              x.Date == datetoEnter &&
                                              x.Class == flightReq.flyingClass &&
                                              x.Cost == flightReq.flyingCost);

            var mRet = new FlightDto()
            {
                FlightCode = flight.FlightCode,
                From = flight.From,
                To = flight.To,
                Date = flight.Date.ToString("dd/MM/yyyy"),
                Time = flight.Time.ToString("HH:mm"),
                Class = flight.Class,
                Cost = flight.Cost,
                NumberOfSeats = flight.NumberOfSeats,
                Price = flight.Price
            };
            return mRet;
        }        
    }
}