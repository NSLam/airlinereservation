﻿using AirlineReservation.DAL.Models;
using AirlineReservation.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AirlineReservation.DAL
{
    public static class BookingDAL
    {
        private static readonly AirlineReservationContext _db = new AirlineReservationContext();

        public static IList<BookingDto> GetListBooking()
        {
            var bookingList = new List<BookingDto>();

            var booking = _db.Booking.Select(x => x);

            foreach(var i in booking.ToList())
            {
                var f = new BookingDto
                {
                    BookingCode = i.BookingCode,
                    TimeReservation = i.TimeReservation.ToString("dd/MM/yyyy HH:mm"),
                    TotalAmount = i.TotalAmount,
                    Status = i.Status
                };

                bookingList.Add(f);
            }

            return bookingList;
        }

        public static string AddBooking(string bookingCode ,FlightReq flightReq)
        {
            var b = new Booking()
            {
                BookingCode = bookingCode,
                TimeReservation = DateTime.Now,
                TotalAmount = flightReq.flyingPrice,
                Status = 0,
            };
            _db.Booking.Add(b);
            _db.SaveChanges();

            return b.BookingCode;
        }

        public static string AddBookingReturn(string bookingCode, FlightReq flightReq)
        {
            var booking = _db.Booking.FirstOrDefault(x => x.BookingCode == bookingCode);

            booking.TotalAmount = booking.TotalAmount + flightReq.flyingPrice;

            _db.SaveChanges();

            return booking.BookingCode;
        }

        public static BookingDto ConfirmBooking(BookingReq bookingReq)
        {
            var booking = _db.Booking.FirstOrDefault(x => x.BookingCode == bookingReq.BookingCode);

            var mRet = new BookingDto()
            {
                BookingCode = booking.BookingCode,
                TimeReservation = booking.TimeReservation.ToString("dd/MM/yyyy HH:mm"),
                TotalAmount = booking.TotalAmount,
                Status = booking.Status
            };
            return mRet;
        }

        public static string PaymentBooking(BookingDto bookingReq)
        {
            var booking = _db.Booking.FirstOrDefault(x => x.BookingCode == bookingReq.BookingCode);

            booking.Status = 1;

            _db.SaveChanges();

            return booking.BookingCode;
        }
    }
}